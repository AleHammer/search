#include <string>

using namespace std;

int search(string searched, string pattern)
{
	int finalIndex = -1;
	bool found = false;
	
	for(int i = 0; i <= (searched.size()-pattern.size());i++)
	{
		if(searched == "" || pattern == "")//ends the loop if either searched or pattern are empty strings
			break;
			
		if(searched[i] == pattern[0])
		{
			found = true;
			for(int j = 1; j < pattern.size(); j++)
			{
				if(searched[j + i] != pattern[j])
				{
					found = false;
					break;
				}
			}
			
			if(found == true)
			{
				finalIndex = i;
				break;
			}					
		}
	}
	
	return finalIndex;
}
